package com.aswdc.matrimony;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cvActRegistration)
    CardView cvActRegistration;
    @BindView(R.id.cvActList)
    CardView cvActList;
    @BindView(R.id.cvActFavotire)
    CardView cvActFavotire;
    @BindView(R.id.cvActSearch)
    CardView cvActSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpActionBar();
    }

    void setUpActionBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.lbl_dashboard);
    }

    @OnClick(R.id.cvActRegistration)
    public void onCvActRegistrationClicked() {

    }

    @OnClick(R.id.cvActList)
    public void onCvActListClicked() {
    }

    @OnClick(R.id.cvActFavotire)
    public void onCvActFavotireClicked() {
    }

    @OnClick(R.id.cvActSearch)
    public void onCvActSearchClicked() {
    }
}