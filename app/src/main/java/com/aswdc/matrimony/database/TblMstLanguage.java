package com.aswdc.matrimony.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;

public class TblMstLanguage extends MyDatabase {

    public static final String TABLE_NAME = "TblMstLanguage";
    public static final String LANGUAGE_ID = "LanguageID";
    public static final String NAME = "Name";

    public TblMstLanguage(Context context) {
        super(context);
    }

    public ArrayList<HashMap<String, Object>> getLanguage() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<HashMap<String, Object>> list = new ArrayList();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i=0;i<cursor.getCount();i++) {
            HashMap<String, Object> map = new HashMap<>();
            map.put(LANGUAGE_ID, cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            map.put(NAME, cursor.getString(cursor.getColumnIndex(NAME)));
            list.add(map);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
}