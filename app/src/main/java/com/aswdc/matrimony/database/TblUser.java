package com.aswdc.matrimony.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.matrimony.model.UserModel;

import java.util.ArrayList;

public class TblUser extends MyDatabase {

    public static final String TABLE_NAME = "TblUser";
    public static final String USER_ID = "UserId";
    public static final String NAME = "Name";
    public static final String FATHER_NAME = "FatherName";
    public static final String SUR_NAME = "SurName";
    public static final String GENDER = "Gender";
    public static final String HOBBIES = "Hobbies";
    public static final String DOB = "Dob";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String LANGUAGE_ID = "LanguageID";
    public static final String CITY_ID = "CityID";

    public TblUser(Context context) {
        super(context);
    }

    public ArrayList<UserModel> getUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < list.size(); i++) {
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public UserModel getCreatedModelUsingCursor(Cursor cursor) {
        UserModel model = new UserModel();
        model.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        model.setCityID(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        model.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
        model.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
        model.setDob(cursor.getString(cursor.getColumnIndex(DOB)));
        model.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        model.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
        model.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        model.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        model.setSurName(cursor.getString(cursor.getColumnIndex(SUR_NAME)));
        return model;
    }

    public UserModel getUserById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        UserModel model = new UserModel();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + USER_ID + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model = getCreatedModelUsingCursor(cursor);
        cursor.close();
        db.close();
        return model;
    }

    public ArrayList<UserModel> getUserListByGender(int gender) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + GENDER + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(gender)});
        cursor.moveToFirst();
        for (int i = 0; i < list.size(); i++) {
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public long insertUser(String name, String fatherName, String surName, int gender, String hobbies,
                           String date, String phoneNumber, int languageID, int cityID) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(FATHER_NAME, fatherName);
        cv.put(SUR_NAME, surName);
        cv.put(GENDER, gender);
        cv.put(HOBBIES, hobbies);
        cv.put(DOB, date);
        cv.put(PHONE_NUMBER, phoneNumber);
        cv.put(LANGUAGE_ID, languageID);
        cv.put(CITY_ID, cityID);
        long lastInsertedID = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertedID;
    }

    public int updateUserById(String name, String fatherName, String surName, int gender, String hobbies,
                              String date, String phoneNumber, int languageID, int cityID, int userId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(NAME, name);
        cv.put(FATHER_NAME, fatherName);
        cv.put(SUR_NAME, surName);
        cv.put(GENDER, gender);
        cv.put(HOBBIES, hobbies);
        cv.put(DOB, date);
        cv.put(PHONE_NUMBER, phoneNumber);
        cv.put(LANGUAGE_ID, languageID);
        cv.put(CITY_ID, cityID);
        int lastUpdatedId = db.update(TABLE_NAME, cv, USER_ID + " = ?", new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdatedId;
    }

    public int deleteUserById(int userId) {
        SQLiteDatabase db = getWritableDatabase();
        int deletedUserID = db.delete(TABLE_NAME, USER_ID + " = ?", new String[]{String.valueOf(userId)});
        db.close();
        return deletedUserID;
    }
}