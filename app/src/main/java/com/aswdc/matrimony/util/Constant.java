package com.aswdc.matrimony.util;

public class Constant {

    public static final int MALE = 1;
    public static final int FEMALE = 2;
    public static final int OTHER = 3;
}
